# React Native Middleware

## Setup

**Required**

1. Node.js + npm - [https://nodejs.org/fr/](https://nodejs.org/fr/)
2. React Native project - [https://reactnative.dev](https://reactnative.dev)

**Install the dependency**

You should execute this command in your React Native project:

```bash
npm i react-native-middleware
```

## How it works?

### Design

The middleware makes it easy to pass data between the React Native App and Websites or encapsulated applications (such like JS bundle).

![Transmission App/Middleware](https://gitlab.com/Yarflam/react-native-middleware/-/raw/main/plan.svg)

<div class="text-center">Data transmission - emit/receive or request method</div>

### Practical applications

Functionalities:

-   **Network status**: detect connection/disconnection with internet.
-   **Storage**: keep saved your data in the phone storage.
-   **Notification**: notify users of news.
-   **Hardware access**: it facilitates the implementation of sensors.

## Usage

Test code is available: [test/App.js](https://gitlab.com/Yarflam/react-native-middleware/-/blob/main/test/App.js)

### Step 1 - Import the library

Like any library

```javascript
import { WebView } from './react-native-middleware';
```

### Step 2 - Add the WebView

Define your WebView with its URL, its style ... [see more](https://github.com/react-native-webview/react-native-webview)

```javascript
return (
    <View>
        <WebView source={'https://gitlab.com/Yarflam/react-native-middleware'} />
    </View>
);
```

### Step 3 - Ping/Pong props

The WebView accepts two additional options:

-   **receiveMsg**: link your own function to data exchange (`@args: topic:String, payload:any, sendMsg:Function`)
-   **scripts**: run scripts when starting the WebView.

Try this:

```javascript
export default function App() {
    /* Capture the messages (application side) */
    const receiveMsg = (topic, payload, sendMsg) => {
        if (topic === 'giveAnswer') {
            /* Replying */
            sendMsg('answer', payload === 'H2G2' ? 42 : 1337);
        }
    };
    /* Your WebView */
    return (
        <View>
            <WebView
                source={'https://gitlab.com/Yarflam/react-native-middleware'}
                receiveMsg={receiveMsg}
                scripts={[
                    function({ sendMsg, receiveMsg }) {
                        /* Receive a message (page side) */
                        receiveMsg('answer', answer => {
                            document.querySelector('body').innerHTML = answer;
                        });
                        /* Send a message (page side) */
                        sendMsg('giveAnswer', 'H2G2');
                    }
                ]}
            />
        </View>
    );
}
```

### Step 4 - Page side

The middleware set a global variable in the page:

```javascript
window.reactNativeMiddleware; // platform:Object, sendMsg:Function, receiveMsg:Function, request:Function
```

Or you can wait for the instance to initialize:

```javascript
window.addEventListener('reactNativeMiddleware', e => {
    var reactNativeMiddleware = e.detail;
    // Do something - ex: call pending functions
});
```

`reactNativeMiddleware.platform:0bject` - Get information about the device

```JSON
{
    "os": "android",
    "version": 30
}
```

`reactNativeMiddleware.sendMsg:Function` - Send a message to React Native application

```javascript
reactNativeMiddleware.sendMsg('topic', payload);
```

`reactNativeMiddleware.receiveMsg:Function` - Receive an answer from React Native application

```javascript
reactNativeMiddleware.receiveMsg('topic', payload => {
    // Do something
});
```

`reactNativeMiddleware.request:Function` - Send/Receive a request

```javascript
reactNativeMiddleware.request('topic', payload, answer => {
    // Do something
});
```

## Versions

-   **v1.0.5**: Support custom event at startup
-   **v1.0.4**: Fix README - npm/gitlab linking
-   **v1.0.3**: Write README + proper definition of the license
-   **v1.0.2**: Add _.npmignore_ file
-   **v1.0.1**: Fix package.json (dev dependencies)
-   **v1.0.0**: First version

## Author

-   Yarflam - _initial worker_

## Licence

MIT License (MIT) - [read more](https://gitlab.com/Yarflam/react-native-middleware/-/blob/main/LICENCE)
