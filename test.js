const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');

/* Module to execute bash command */
function bash(cmd, debug = true) {
    return new Promise(resolve => {
        if (debug) console.log(`#> ${cmd}`);
        exec(cmd, (err, stdout, stderr) => {
            let json = { success: false, error: stderr, data: null };
            if (err) {
                json.code = err.code;
                if (debug) console.log(stderr);
            } else {
                json.success = true;
                json.data = stdout;
                if (debug) console.log(stdout);
            }
            resolve(json);
        });
    });
}

/* Install a React Native project */
(async function main() {
    const currentFolder = path.resolve(__dirname);
    /* Install a basic React Native application */
    if (!fs.existsSync('./mon-app')) {
        await bash('echo "Y" | ./node_modules/.bin/create-react-native-app mon-app');
        await bash('cd ./mon-app && npm i @react-native-community/netinfo react-native-webview');
        await bash(`cd ./mon-app && npm i ${currentFolder}`);
    }
    /* Copy the file */
    fs.readdirSync('./test').forEach(name => {
        console.log(`Copy ${name}`);
        fs.copyFileSync(`./test/${name}`, `./mon-app/${name}`);
    });
})();
