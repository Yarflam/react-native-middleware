"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Tools = _interopRequireDefault(require("./Tools"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/* Browser side */
function Middleware(platform, scripts, _getTypeOf) {
  /* [Custom] Receive message from App */
  var subscribers = {};

  function receiveMsg(topic, callback) {
    var once = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    /* New slot */
    if (_getTypeOf(subscribers[topic]) === 'undefined') subscribers[topic] = [];
    /* Add the callback */

    subscribers[topic].push({
      callback: callback,
      once: once
    });
  }
  /* [Native] Receive message from App */


  function _receiveMsg(data) {
    /* Extract the JSON data */
    try {
      data = JSON.parse(data);
    } catch (e) {
      return;
    }
    /* Extract topic and payload */


    var topic, payload;

    if (data !== null && _getTypeOf(data) === 'object' && _getTypeOf(data.topic) === 'string' && _getTypeOf(data.payload) !== 'undefined') {
      topic = data.topic;
      payload = data.payload;
    } else return;
    /* Middleware instructions */


    if (topic === '$Middleware') {
      /* Ping/Pong */
      if (payload === 'ping') sendMsg('$Middleware', 'pong');
      /* Advance mode */

      if (payload !== null && _getTypeOf(payload) === 'object' && _getTypeOf(payload.action) === 'string') {
        /* Request */
        var dataRequest = payload;

        if (dataRequest.action === 'request' && _getTypeOf(requests[dataRequest.id]) === 'function') {
          /* Propagate */
          requests[dataRequest.id](dataRequest.payload);
          delete requests[dataRequest.id];
        }

        return;
      }
    }
    /* Call the subscribers */


    if (_getTypeOf(subscribers[topic]) !== 'undefined') {
      subscribers[topic].forEach(function (_ref) {
        var callback = _ref.callback;
        return callback(payload);
      });
      subscribers[topic] = subscribers[topic].filter(function (_ref2) {
        var once = _ref2.once;
        return Boolean(once);
      });
    }
  }

  (platform.os === 'ios' ? window : document).addEventListener('message', function (event) {
    _receiveMsg(event.data);
  });
  /* [Custom] Send request to App */

  var requests = {};
  var uniqid = new Date().getTime();

  function request(topic, payload, callback) {
    if (_getTypeOf(callback) !== 'function') return;
    /* New request */

    uniqid++;
    requests[String(uniqid)] = callback;
    /* Send the request */

    sendMsg('$Middleware', {
      action: 'request',
      id: String(uniqid),
      topic: topic,
      payload: payload
    });
  }
  /* [Native] Send message to App */


  function sendMsg(topic, payload) {
    window.ReactNativeWebView.postMessage(JSON.stringify({
      topic: topic,
      payload: payload
    }));
  }
  /*
   *  Boot
   */

  /* Give access to middleware in the page */


  var engine = {
    platform: platform,
    sendMsg: sendMsg,
    receiveMsg: receiveMsg,
    request: request
  };
  window.reactNativeMiddleware = engine;
  /* Execute the scripts */

  scripts.forEach(function (script) {
    return script(engine);
  });
  /* Send an event */

  window.dispatchEvent(new CustomEvent('reactNativeMiddleware', {
    detail: engine,
    bubbles: true,
    cancelable: true,
    composed: false
  }));
  /* First initialization */

  sendMsg('$Middleware', 'ping');
}
/* Export */


var _default = {
  boot: function boot(platform) {
    var scripts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    return "(".concat(Middleware.toString(), ")(").concat(JSON.stringify(platform), ", [").concat(scripts.filter(function (script) {
      return Boolean(script);
    }).map(function (script) {
      return script.toString();
    }).join(','), "], elem => typeof elem)");
  },
  getTestPage: function getTestPage() {
    return "data:text/html;base64,".concat(_Tools["default"].b64encode('<!DOCTYPE html><html><head><meta charset="utf-8"/>' + '<title>Test</title></head><body>' + '<h1>Test WebView</h1><p>Initial content</p>' + '</body></html>'));
  }
};
exports["default"] = _default;