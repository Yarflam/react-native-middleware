"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Tools = /*#__PURE__*/function () {
  function Tools() {
    _classCallCheck(this, Tools);
  }

  _createClass(Tools, null, [{
    key: "b64encode",
    value: function b64encode(c) {
      var o, r, a, l, i, t;

      for (a = Tools.b64alpha, o = '', r = 0, i = 0; (l = c.charCodeAt(i), l >= 0) || (l = 0, c[i - 1] && i % 3) || (a = '=', r = l = 0, o.length % 4); !(i % 3) && c[i - 1] ? (o += a[r], r = 0) : 1) {
        t = (i % 3 + 1) * 2, o += a[r + (l >> t)], r = l - (l >> t << t) << 6 - t, i++;
      }

      return o;
    }
  }, {
    key: "b64decode",
    value: function b64decode(c) {
      var o, r, a, l, i, t;

      for (a = Tools.b64alpha, o = '', r = 0, i = 0; l = a.indexOf(c[i]), l >= 0; i++) {
        t = (6 - i % 4 * 2) % 6, i % 4 ? (o += String.fromCharCode(r + (l >> t)), r = l - (l >> t << t) << 8 - t) : r = l << 2;
      }

      return o;
    }
  }]);

  return Tools;
}();

_defineProperty(Tools, "b64alpha", 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/');

var _default = Tools;
exports["default"] = _default;