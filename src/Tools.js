class Tools {
    static b64alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

    static b64encode(c) {
        let o, r, a, l, i, t;
        for (
            a = Tools.b64alpha, o = '', r = 0, i = 0;
            ((l = c.charCodeAt(i)), l >= 0) ||
            ((l = 0), c[i - 1] && i % 3) ||
            ((a = '='), (r = l = 0), o.length % 4);
            !(i % 3) && c[i - 1] ? ((o += a[r]), (r = 0)) : 1
        ) {
            (t = ((i % 3) + 1) * 2), (o += a[r + (l >> t)]), (r = (l - ((l >> t) << t)) << (6 - t)), i++;
        }
        return o;
    }

    static b64decode(c) {
        let o, r, a, l, i, t;
        for (a = Tools.b64alpha, o = '', r = 0, i = 0; (l = a.indexOf(c[i])), l >= 0; i++) {
            (t = (6 - (i % 4) * 2) % 6),
            i % 4
                ? ((o += String.fromCharCode(r + (l >> t))), (r = (l - ((l >> t) << t)) << (8 - t)))
                : (r = l << 2);
        }
        return o;
    }
}

export default Tools;
