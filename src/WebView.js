import React, { Component } from 'react';
import { Platform } from 'react-native';
import Middleware from './Middleware';
import WebViewNative from 'react-native-webview';

class WebView extends Component {
    constructor(props) {
        super(props);
        this.webview = null;
    }

    /* Receive message from Middleware */
    receiveMsg = async event => {
        /* Extract the JSON data */
        let data = event.nativeEvent.data;
        try {
            data = JSON.parse(data);
        } catch (e) {
            return;
        }
        /* Extract topic and payload */
        let topic, payload;
        if (
            data !== null &&
            typeof data === 'object' &&
            typeof data.topic === 'string' &&
            typeof data.payload !== 'undefined'
        ) {
            topic = data.topic;
            payload = data.payload;
        } else return;
        /* Middleware instructions */
        if (topic === '$Middleware') {
            /* Ping/Pong */
            if (payload === 'ping') this.sendMsg('$Middleware', 'pong');
            /* Advance mode */
            if (payload !== null && typeof payload === 'object' && typeof payload.action === 'string') {
                /* Request */
                if (payload.action === 'request' && typeof this.props.receiveMsg === 'function') {
                    /* Propagate */
                    const dataRequest = payload;
                    this.props.receiveMsg(dataRequest.topic, dataRequest.payload, (topic, payload) => {
                        if (topic !== dataRequest.topic) return;
                        /* Reply */
                        this.sendMsg('$Middleware', {
                            action: 'request',
                            id: dataRequest.id,
                            topic,
                            payload
                        });
                    });
                }
                return;
            }
        }
        /* Propagate */
        if (typeof this.props.receiveMsg === 'function') {
            this.props.receiveMsg(topic, payload, (topic, payload) => this.sendMsg(topic, payload));
        }
    };

    /* Send message to Middleware */
    sendMsg = (topic, payload) => this.webview.postMessage(JSON.stringify({ topic, payload }));

    /* Main WebView */
    render() {
        const { source, style, scripts, ...overrides } = this.props;
        return (
            <WebViewNative
                style={style}
                source={{ uri: source }}
                useWebkit={true}
                originWhitelist={['*']}
                javaScriptEnabled={true}
                messagingEnabled={true}
                onMessage={this.receiveMsg}
                injectedJavaScript={Middleware.boot(
                    { os: Platform.OS, version: Platform.Version },
                    scripts || []
                )}
                bounces={false}
                scrollEnabled={false}
                ref={ref => (this.webview = ref)}
                {...Object.entries(overrides || {})
                    .filter(([key]) => key !== 'receiveMsg')
                    .reduce((accum, [key, value]) => {
                        accum[key] = value;
                        return accum;
                    }, {})}
            />
        );
    }
}

export default WebView;
