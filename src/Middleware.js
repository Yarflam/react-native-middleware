import Tools from './Tools';

/* Browser side */
function Middleware(platform, scripts, _getTypeOf) {
    /* [Custom] Receive message from App */
    let subscribers = {};
    function receiveMsg(topic, callback, once = false) {
        /* New slot */
        if (_getTypeOf(subscribers[topic]) === 'undefined') subscribers[topic] = [];
        /* Add the callback */
        subscribers[topic].push({ callback, once });
    }

    /* [Native] Receive message from App */
    function _receiveMsg(data) {
        /* Extract the JSON data */
        try {
            data = JSON.parse(data);
        } catch (e) {
            return;
        }
        /* Extract topic and payload */
        let topic, payload;
        if (
            data !== null &&
            _getTypeOf(data) === 'object' &&
            _getTypeOf(data.topic) === 'string' &&
            _getTypeOf(data.payload) !== 'undefined'
        ) {
            topic = data.topic;
            payload = data.payload;
        } else return;
        /* Middleware instructions */
        if (topic === '$Middleware') {
            /* Ping/Pong */
            if (payload === 'ping') sendMsg('$Middleware', 'pong');
            /* Advance mode */
            if (
                payload !== null &&
                _getTypeOf(payload) === 'object' &&
                _getTypeOf(payload.action) === 'string'
            ) {
                /* Request */
                const dataRequest = payload;
                if (dataRequest.action === 'request' && _getTypeOf(requests[dataRequest.id]) === 'function') {
                    /* Propagate */
                    requests[dataRequest.id](dataRequest.payload);
                    delete requests[dataRequest.id];
                }
                return;
            }
        }
        /* Call the subscribers */
        if (_getTypeOf(subscribers[topic]) !== 'undefined') {
            subscribers[topic].forEach(({ callback }) => callback(payload));
            subscribers[topic] = subscribers[topic].filter(({ once }) => Boolean(once));
        }
    }
    (platform.os === 'ios' ? window : document).addEventListener('message', function(event) {
        _receiveMsg(event.data);
    });

    /* [Custom] Send request to App */
    let requests = {};
    let uniqid = new Date().getTime();
    function request(topic, payload, callback) {
        if (_getTypeOf(callback) !== 'function') return;
        /* New request */
        uniqid++;
        requests[String(uniqid)] = callback;
        /* Send the request */
        sendMsg('$Middleware', {
            action: 'request',
            id: String(uniqid),
            topic,
            payload
        });
    }

    /* [Native] Send message to App */
    function sendMsg(topic, payload) {
        window.ReactNativeWebView.postMessage(JSON.stringify({ topic, payload }));
    }

    /*
     *  Boot
     */

    /* Give access to middleware in the page */
    const engine = {
        platform,
        sendMsg,
        receiveMsg,
        request
    };
    window.reactNativeMiddleware = engine;

    /* Execute the scripts */
    scripts.forEach(script => script(engine));

    /* Send an event */
    window.dispatchEvent(
        new CustomEvent('reactNativeMiddleware', {
            detail: engine,
            bubbles: true,
            cancelable: true,
            composed: false
        })
    );

    /* First initialization */
    sendMsg('$Middleware', 'ping');
}

/* Export */
export default {
    boot: (platform, scripts = []) =>
        `(${Middleware.toString()})(${JSON.stringify(platform)}, [${scripts
            .filter(script => Boolean(script))
            .map(script => script.toString())
            .join(',')}], elem => typeof elem)`,
    getTestPage: () => {
        return `data:text/html;base64,${Tools.b64encode(
            '<!DOCTYPE html><html><head><meta charset="utf-8"/>' +
                '<title>Test</title></head><body>' +
                '<h1>Test WebView</h1><p>Initial content</p>' +
                '</body></html>'
        )}`;
    }
};
