import { Platform, StyleSheet, Text, View } from 'react-native';
import { WebView, Middleware } from './react-native-middleware';
import React from 'react';

export default function App() {
    /* Capture the messages */
    const receiveMsg = (topic, payload, sendMsg) => {
        /* Receive pageSize and emit a value */
        if (topic === 'pageSize') {
            // alert(`Page size: ${payload} octets`);
            sendMsg('bodyWrite', payload > 50 ? 'high' : 'low');
        }
        /* Test myRequest */
        if (topic === 'myRequest') {
            sendMsg(topic, 42);
        }
    };
    /* Main render */
    return (
        <View style={styles.container}>
            <WebView
                source={Middleware.getTestPage()}
                styles={styles.webview}
                receiveMsg={receiveMsg}
                scripts={[
                    function({ sendMsg, receiveMsg, request }) {
                        /* Rewrite body */
                        receiveMsg('bodyWrite', content => (document.querySelector('p').innerHTML = content));
                        /* Send the page size */
                        sendMsg('pageSize', document.querySelector('html').innerHTML.length);
                        /* Ask after many seconds */
                        setTimeout(() => {
                            request('myRequest', {}, data => {
                                document.querySelector('p').innerHTML = data;
                            });
                        }, 3000);
                    }
                ]}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#222',
        width: '100%',
        height: '100%'
    },
    webview: {
        width: '100%',
        height: '100%',
        marginTop: Platform.OS === 'ios' ? (Platform.isPad ? '3%' : '8%') : 0
    }
});
